import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

import dtkCore         1.0 as D

Control {

	required property var param;

	background: Rectangle{ color: X.Style.backgroundColor; radius: 4; }

	RowLayout {
		anchors.fill: parent;
		anchors.margins: 10;

		spacing: 20;

		X.LabelCaption { text: param.label; }

        X.CheckComboBox {
            id: _val
            model: ListModel {
            	id: lsl_model
            }
            Layout.fillWidth: true;

			onDisplayTextChanged: {
				param.value = _val.displayText.split(",");
			}

			X.ToolTip {
				parent: _val.parent
				visible: _val.hovered && !_val.pressed
				text: param.doc
			}
		}
	}

	function updateModel()
    {
		lsl_model.clear();

		for(const v of param.list) {
			lsl_model.append({"name": v, "ischecked": param.value.includes(v)})
		}
	}

	Component.onCompleted: {
		updateModel();
		_val.displayText = param.value.toString();

	}
}
