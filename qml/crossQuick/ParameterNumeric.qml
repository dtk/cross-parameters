import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

import dtkCore         1.0 as D

Item {
    id: _root

    required property var param;
    property var value: param ? param.value : 0;
    property var min: param ? param.min : 0;
    property var max: param ? param.max : 0;
    property var decimals: param ? param.decimals : 2;    

    Binding { target: _root.param; property: "value"; value: _root.value}
    Binding { target: _root.param; property: "min"; value: _root.min; when:paramType != "dtkCoreParameterNumeric<bool>"}
    Binding { target: _root.param; property: "max"; value: _root.max; when:paramType != "dtkCoreParameterNumeric<bool>"}
    Binding { target: _root.param; property: "decimals"; value: _root.decimals; when:paramType == "dtkCoreParameterNumeric<double>"}

}