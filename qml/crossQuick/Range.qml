import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

import dtkCore         1.0 as D

Control {

	required property var param;
	property int decimals: param.decimals;

	RowLayout {
		anchors.fill: parent;
		anchors.margins: 10;

		spacing: 20;

		X.LabelCaption { text: param.label; }

        X.RangeSlider {
            id: _val
            from: param.min
            to: param.max
            first.value: param.rmin
            second.value: param.rmax

            Layout.fillWidth: true;

            first.onMoved: {
				param.rmin = first.value
	        }
			second.onMoved: {
				param.rmax = second.value
			}

			X.Label {
				id: _label_first
				text: _val.first.value.toFixed(decimals)
				color: "white"

				anchors.bottom: parent.top;
				x: _val.leftPadding + _val.first.visualPosition * (_val.availableWidth - width)
			}
			X.Label {
				id: _label_second
				text: _val.second.value.toFixed(decimals)
				color: "white"

				anchors.bottom: parent.top;
				x: _val.leftPadding + _val.second.visualPosition * (_val.availableWidth - width)
			}

			X.ToolTip {
				parent: _val.parent
				visible: _val.hovered && !_val.pressed
				text: param.doc
			}

			X.Label {
				text: param.min
				color: "white"

				anchors.top: parent.bottom;
				anchors.left: parent.left;
			}

			X.Label {
				text: param.max
				color: "white"

				anchors.top: parent.bottom;
				anchors.right: parent.right;
			}
		}
	}

	background: Rectangle{ color: X.Style.backgroundColor; radius: 4; }
}
