import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

import dtkCore         1.0 as D

Control {
	id: _root

	required property var param;
	required property var paramType

	Component {
		id: slider
		Slider {
			id: _slid

			ParameterNumeric {
				id: _param
				param: _root.param
				value: _slid.value
			}

			from: (_param  && paramType != "dtkCoreParameterNumeric<bool>") ? _param.min : 0
			to:   (_param  && paramType != "dtkCoreParameterNumeric<bool>") ? _param.max : 1
			value: _param.param.value;
			stepSize: (!_param.param || _param.param.decimals == undefined) ? 1 : .0/Math.pow(10, param.decimals)

			X.ToolTip {
				parent: _slid.handle
				visible: _slid.hovered && !_slid.pressed
				text: param ? param.doc : ""
			}

			X.Label {
				text: (_param  && paramType != "dtkCoreParameterNumeric<bool>") ? _param.min : ""
				color: "white"

				anchors.top: parent.bottom;
				anchors.left: parent.left;
			}

			TextInput {
				id: _label; 
				anchors.bottom: parent.top;
				x: _slid.leftPadding + _slid.visualPosition * (_slid.availableWidth - width)

				text: _slid.value
				color: "white"
				validator: DoubleValidator{bottom: _param.min; top: _param.max; decimals: (_param.decimals) ? _param.decimals : 0 ; notation: DoubleValidator.StandardNotation}
				onTextChanged: {
                   _slid.value =  parseInt(text);
                }
			}

			X.Label {
				text: (_param  && paramType != "dtkCoreParameterNumeric<bool>") ? _param.max : ""
				color: "white"

				anchors.top: parent.bottom;
				anchors.right: parent.right;
			}

			onValueChanged: {
				_label.text = _slid.value.toFixed(param.decimals);
			}
		}
	}
	Component {
		id: spinbox
		X.ScientificSpinBoxReal {
			id: _spin;

			from: param.min
			to: param.max
			decimals: param.decimals
			increment: 1.0/Math.pow(10, param.decimals)

			value: param.value
			notation: DoubleValidator.StandardNotation

			onValueChanged: {
				param.value = _spin.value;
			}

		}
	}

	RowLayout {
		anchors.fill: parent;
		anchors.margins: 10;

		spacing: 20;

		X.LabelCaption { text: param? param.label : "" }

		Loader {
			//sourceComponent: paramType == "dtkCoreParameterNumeric<double>" ? spinbox : slider
			sourceComponent: slider
			Layout.fillWidth: true
		}
	}

	background: Rectangle{ color: X.Style.backgroundColor; radius: 4; }
}
