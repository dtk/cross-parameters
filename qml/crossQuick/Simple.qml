import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

import dtkCore         1.0 as D

Control {
	id: _root
	required property var param;

	RowLayout {
		anchors.fill: parent;
		anchors.margins: 10;

		spacing: 20;

		X.LabelCaption { text: param.label; }

        X.PlaceholderText {
            id: _val
            text: param.value;
            Layout.fillWidth: true;

			onTextChanged: {
				param.value = _val.text; //TODO conversion for non string parameter?
			}

			X.ToolTip {
				parent: _val.parent
				visible: _root.hovered && !_root.pressed
				text: param.doc
			}
		}
	}

	background: Rectangle{ color: X.Style.backgroundColor; radius: 4; }
}
