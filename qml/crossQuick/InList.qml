import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

import dtkCore         1.0 as D

Control {

	required property var param;

	RowLayout {
		anchors.fill: parent;
		anchors.margins: 10;

		spacing: 20;

		X.LabelCaption { text: param.label; }

        X.ComboBox {
            id: _val
            model: param.list 
            currentIndex: param.index

            Layout.fillWidth: true;

            onCurrentIndexChanged: {
				param.index = _val.currentIndex
            }

			X.ToolTip {
				parent: _val.parent
				visible: _val.hovered && !_val.pressed
				text: param.doc
			}
		}
	}

	background: Rectangle{ color: X.Style.backgroundColor; radius: 4; }
}
