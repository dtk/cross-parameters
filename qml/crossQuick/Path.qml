import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import Qt.labs.platform  1.0 as P

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

import dtkCore         1.0 as D

Control {
	id: _root
	required property var param;

	RowLayout {
		anchors.fill: parent;
		anchors.margins: 10;

		spacing: 20;

		X.LabelCaption { text: param.label; }

        X.Button {
            id: _val
            text: param.path;
            Layout.fillWidth: true;

			//onpathChanged: {}

            onClicked: {
                _file_dialog.open();
            }

			X.ToolTip {
				parent: _val.parent
				visible: _root.hovered && !_root.pressed
				text: param.doc
			}
		}
	}

    P.FileDialog {
        id: _file_dialog;

        currentFile: param.baseName;
        folder: param.dirName;
        nameFilters: param.filters

        modality: Qt.NonModal;

        onAccepted: {
            _val.text = param.path;
            param.path = _file_dialog.file;
        }
    }

	background: Rectangle{ color: X.Style.backgroundColor; radius: 4; }
}
