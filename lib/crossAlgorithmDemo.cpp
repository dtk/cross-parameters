// crossAlgorithmDemo.cpp
//

#include "crossAlgorithmDemo.hpp"

using namespace dtk;

class crossAlgorithmDemoPrivate
{
public:
    dtkCoreParameters params;
    QString input;
    QString output;
};

crossAlgorithmDemo::crossAlgorithmDemo(void) : d(new crossAlgorithmDemoPrivate)
{
    d_real *r = new d_real("frequency", 10, 0, 1e8, 6, "Frequency in Hz");
    d_int *i = new d_int("size", 256, 5, 1024, "Number of characters of the output");
    d_int *b = new d_int("blank", 2, 0, 5, "Number of blank space between strings");
    d_path *pp = new d_path("the_path", "/tmp", {""}, "the path chosen");
    d_string *s = new d_string("a_string", "miaou", "this is a string documentation");

    d_range_real *rr = new d_range_real("range", {0.2, 0.5}, 0.0, 10., "I am a range real parameter");

    d_inliststring *inliststring = new d_inliststring("listofstrings", "SecondString", {"FirstString", "SecondString", "ThirdString"}, "I am a list of strings");
    
    d_inliststringlist *inliststringlist = new d_inliststringlist("listofstringslist", {"aa", "bb"}, {"aa", "bb", "cc", "dd"}, "I am a list string list");

    d->params["r_freq"] = r;
    d->params["p_path"] = pp;
    d->params["i_size"] = i;
    d->params["b_blank"] = b;
    d->params["r_real"] = rr;
    d->params["s_string"] = s;
    d->params["t_liststring"] = inliststring;
    d->params["w_inliststringlist"] = inliststringlist;
}

crossAlgorithmDemo::~crossAlgorithmDemo(void)
{
    delete d;
}

void crossAlgorithmDemo::setInput(const QString& i)
{
    d->input = i;
}

QString crossAlgorithmDemo::readInput()
{
    return d->input;
}

void crossAlgorithmDemo::run(void)
{
    double r = *reinterpret_cast<d_real*>(d->params["r_freq"]);
    d->output = QString("%1 is loaded at a fake frequency of %2 Hz.").arg(d->input).arg(r);

    QList<int> blank_pos;
    for (auto it = d->output.begin(); it != d->output.end(); ++it) {
        if (*it == ' ') {
            blank_pos << d->output.indexOf(*it);
        }
    }
    int b = *reinterpret_cast<d_int*>(d->params["b_blank"]);
    for (auto rit = blank_pos.rbegin(); rit != blank_pos.rend(); ++rit) {
        int pos = *rit;
        for (int j = 0; j < b; ++j) {
            d->output.insert(pos, QString(" "));
        }
    }

    d_inliststringlist ll_temp = *reinterpret_cast<d_inliststringlist*>(d->params["w_inliststringlist"]);
    d_inliststring l_temp = *reinterpret_cast<d_inliststring*>(d->params["t_liststring"]);

    d_range_real tmp = *reinterpret_cast<d_range_real*>(d->params["r_real"]);
    std::array<double,2> val = tmp.value();

    qDebug() << "input" << d->input;
    qDebug() << "r_freq"  << qSetRealNumberPrecision(6) << r;
    qDebug() << "b_blank" << b;
    qDebug() << "t_liststring" <<  l_temp.value();
    qDebug() << "w_inliststringlist" <<  ll_temp.value();
    qDebug() << "r_real " << val[0] << " " << val[1];
    
}

QString crossAlgorithmDemo::output(void) const
{
    int size = *reinterpret_cast<d_int*>(d->params["i_size"]);
    return d->output.left(size);
}

dtkCoreParameters crossAlgorithmDemo::parameters(void) const
{
    return d->params;
}

//
//  ends of crossAlgorithmDemo.cpp
