// crossAlgorithmCommand.hpp
//

#pragma once

#include <QtQml>

class crossAlgorithmCommand : public QObject
{
	Q_OBJECT

    Q_PROPERTY(QJSValue collection READ collection NOTIFY collectionChanged)
    Q_PROPERTY(QString input READ input WRITE setInput NOTIFY inputChanged)
    Q_PROPERTY(QString output READ output NOTIFY outputChanged)

public:
	crossAlgorithmCommand(void);
	~crossAlgorithmCommand(void);

	signals:
	void collectionChanged(void);
	void inputChanged(void);
	void outputChanged(void);

public:
	QJSValue collection(void);

	QString input(void) const;
	void setInput(QString&);

	QString output(void);

	Q_INVOKABLE void run(void);

private:
	class crossAlgorithmCommandPrivate *d = nullptr;
};

//
// end of crossAlgorithmCommand.hpp
