// crossAlgorithmDemo.hpp
//

#pragma once

#include <dtkCore>

class crossAlgorithmDemo
{
public:
     crossAlgorithmDemo(void);
    ~crossAlgorithmDemo(void);

    void setInput(const QString&);
    QString readInput();

    void run(void);

    QString output(void) const;

    dtkCoreParameters parameters(void) const;

private:
    class crossAlgorithmDemoPrivate *d = nullptr;
};

//
// end of crossAlgorithmDemo.hpp
