//	crossAlgorithmCommand.cpp
//

#include "crossAlgorithmCommand.hpp"

#include "crossAlgorithmDemo.hpp"


class crossAlgorithmCommandPrivate
{
public: 
	dtkCoreParameterCollection m_collection;
	crossAlgorithmDemo m_algorithm;
};

crossAlgorithmCommand::crossAlgorithmCommand(void): d(new crossAlgorithmCommandPrivate)
{
	
	d->m_collection =  d->m_algorithm.parameters();
}

crossAlgorithmCommand::~crossAlgorithmCommand(void)
{
	delete d;
}

QJSValue crossAlgorithmCommand::collection(void)
{
	return d->m_collection.toJSValue(this);
}

QString crossAlgorithmCommand::input(void) const
{
	return d->m_algorithm.readInput();
}

void crossAlgorithmCommand::setInput(QString& i)
{
	d->m_algorithm.setInput(i);
	emit inputChanged();
}

QString crossAlgorithmCommand::output(void)
{
	return d->m_algorithm.output();
}

Q_INVOKABLE void crossAlgorithmCommand::run(void)
{
    d->m_algorithm.run();
}

//
// end of crossAlgorithmCommand.cpp.

