#include "crossParameters.hpp"
#include "crossAlgorithmCommand.hpp"

#include <dtkCore>

void cross_parameters_initialise(void)
{
    qmlRegisterUncreatableType<dtkCoreParameterObject>("dtkCore", 1, 0, "Parameter", "");
    qmlRegisterUncreatableType<dtkCoreParameterNumericIntObject>("dtkCore", 1, 0, "NumericInt", "");
    qmlRegisterUncreatableType<dtkCoreParameterNumericRealObject>("dtkCore", 1, 0, "NumericReal", "");
    qmlRegisterUncreatableType<dtkCoreParameterStringObject>("dtkCore", 1, 0, "String", "");
    qmlRegisterUncreatableType<dtkCoreParameterInListStringObject>("dtkCore", 1, 0, "InListString", "");
    qmlRegisterUncreatableType<dtkCoreParameterInListStringListObject>("dtkCore", 1, 0, "InListStringList", "");
    //TODO path
    qmlRegisterUncreatableType<dtkCoreParameterRangeRealObject>("dtkCore", 1, 0, "RangeReal", "");

    qmlRegisterType<crossAlgorithmCommand>("crossParameters", 1, 0, "CrossAlgorithmCommand");

}

// // /////////////////////////////////////////////////////////////////////
// // Just givin' a shot ....
// // /////////////////////////////////////////////////////////////////////

// #include <dtkCore/dtkCoreParameterObject.h>

// // Q_DECLARE_METATYPE(dtkCoreParameterObject)
// //Q_DECLARE_METATYPE(dtkCoreParameterObject*)


// class crossParameterCollection
// {
// public:
//      crossParameterCollection(void) = default;
//      crossParameterCollection(QObject *object, const dtkCoreParameters& map) : m_object(object), m_map(map) {}
//     ~crossParameterCollection(void) = default;

//     void init(QObject *object, const dtkCoreParameters& map)
//     {
//         m_object = object;
//         m_map = map;
//     }

//     operator QJSValue() const
//     {
//         if (!m_built) {
//             QQmlEngine& js_engine = *(QQmlEngine::contextForObject(m_object)->engine());
//             m_collection = js_engine.newObject();
//             for (auto it = m_map.begin(); it != m_map.end(); ++it) {
//                 m_collection.setProperty(it.key(), js_engine.newQObject(it.value()->object()));
//             }
//             m_built = true;
//         }
//         return m_collection;
//     }

// private:
//     mutable bool m_built = false;
//     QObject *m_object = nullptr;
//     dtkCoreParameters m_map;
//     mutable QJSValue m_collection;
// };


// // /////////////////////////////////////////////////////////////////////

// class Algorithm : public QObject
// {
//     Q_OBJECT
//     Q_PROPERTY(dtkCoreParameterNumericIntObject *param READ param NOTIFY paramChanged)

//     Q_PROPERTY(QVariantMap  parameters    READ parameters    NOTIFY parametersChanged)
//     Q_PROPERTY(QVariantList parameterList READ parameterList NOTIFY parametersChanged)
//     Q_PROPERTY(QMap<QString, QVariant> parametersOnceMore READ parametersOnceMore NOTIFY parametersChanged)
//     //Q_PROPERTY(QList<dtkCoreParameterObject *> parametersTwiceMore READ parametersTwiceMore NOTIFY parametersChanged)
//     Q_PROPERTY(QHash<QString, dtkCoreParameterObject *> parametersTwiceMore READ parametersTwiceMore NOTIFY parametersChanged)

//     Q_PROPERTY(QJSValue collection READ collection NOTIFY collectionChanged)

// public:
//   Algorithm(void);
//   ~Algorithm(void);

//   signals:
//   void paramChanged(void);
//   void parametersChanged(void);
//   void collectionChanged(void);

// public:
//     Q_INVOKABLE dtkCoreParameterObject *paramValue(const QString& key) {
//         return m_objects_twice_more.value(key);
//     }

//     QJSValue collection(void) const;

// public:
//     dtkCoreParameterNumericIntObject *param(void);

//     QVariantMap  parameters(void) const;
//     QVariantList parameterList(void) const;
//     QMap<QString, QVariant> parametersOnceMore(void) const;
//     //QList<dtkCoreParameterObject *> parametersTwiceMore(void) const;
//     QHash<QString, dtkCoreParameterObject *> parametersTwiceMore(void) const;

// private:
//     dtk::d_int _param;
//     dtkCoreParameters m_params;
//     QVariantMap m_objects;
//     QMap<QString, dtkCoreParameterObject *> m_objects_more;
//     //QList<dtkCoreParameterObject *> m_objects_twice_more;
//     QHash<QString, dtkCoreParameterObject *> m_objects_twice_more;
//     mutable QJSValue m_map;
//     crossParameterCollection m_collection;
// };

// Algorithm::Algorithm(void)
// {
//     this->_param = 42;
//     this->_param.setMin(0);
//     this->_param.setMax(50);
//     this->_param.setDocumentation("This is the doc of int parameter");
//     this->_param.setLabel("d_int");

//     m_params["int"] = &(_param);
//     m_params["real"] = new dtk::d_real("d_real", 3.14159, -3.1, 4.1, "This is the doc of real parameter");

//     for (auto it = m_params.begin(); it != m_params.end(); ++it) {
//         auto v = QVariant::fromValue(it.value()->object());
//         //qDebug() << v;
//         m_objects[it.key()] = v ;
//         m_objects_more[it.key()] = it.value()->object();
//         //m_objects_twice_more << it.value()->object();
//         m_objects_twice_more[it.key()]= it.value()->object();
//     }

//     m_collection.init(this, m_params);

//     // qDebug() << Q_FUNC_INFO;
//     // QJSEngine js_engine;
//     // m_map = js_engine.newObject();
//     // for (auto it = m_params.begin(); it != m_params.end(); ++it) {
//     //     m_map.setProperty(it.key(), js_engine.newQObject(it.value()->object()));
//     // }
//     // qDebug() << Q_FUNC_INFO;
//     //qDebug() << m_map;
// }

// Algorithm::~Algorithm(void)
// {
//     dtkCoreParameter *pi = m_params.take("real");
//     delete pi;
// }

// dtkCoreParameterNumericIntObject *Algorithm::param(void)
// {
//     dtkCoreParameterNumericIntObject *param = dynamic_cast<dtkCoreParameterNumericIntObject *>(_param.object());

//     static bool only_once = true;

//     if (only_once) {
//         only_once = false;
//         connect(param, SIGNAL(valueChanged(qlonglong)), this, SIGNAL(paramChanged()));
//     }

//     return param;
// }

// QVariantMap Algorithm::parameters(void) const
// {
//     return m_objects;
// }

// QVariantList Algorithm::parameterList(void) const
// {
//     return m_objects.values();
// }

// QMap<QString, QVariant> Algorithm::parametersOnceMore(void) const
// {
//   return m_objects;
// }

// QJSValue Algorithm::collection(void) const
// {
//     return m_collection;
//     // QQmlEngine& js_engine = *(QQmlEngine::contextForObject(this)->engine());
//     // m_map = js_engine.newObject();
//     // for (auto it = m_params.begin(); it != m_params.end(); ++it) {
//     //     m_map.setProperty(it.key(), js_engine.newQObject(it.value()->object()));
//     // }
//     // return m_map;
// }

// // QList<dtkCoreParameterObject *>  Algorithm::parametersTwiceMore(void) const
// // {
// //   return m_objects_twice_more;
// // }

// QHash<QString, dtkCoreParameterObject *> Algorithm::parametersTwiceMore(void) const
// {
//   return m_objects_twice_more;
// }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////

//#include "crossParameters.moc"
