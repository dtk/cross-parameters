use cpp::cpp;

use qmetaobject::prelude::*;

cpp! {{
#include <crossParameters.hpp>
}}

qrc!(register_resources, "/" {
    "qml/crossQuick/InList.qml",
    "qml/crossQuick/InListStringList.qml",
    "qml/crossQuick/Numeric.qml",
    "qml/crossQuick/Path.qml",
    "qml/crossQuick/ParameterNumeric.qml",
    "qml/crossQuick/Range.qml",
    "qml/crossQuick/Simple.qml",
    "qml/crossQuick/qmldir",
});

pub fn init() {
    register_resources();

    cpp!(unsafe [] {
        cross_parameters_initialise();
    });
}
