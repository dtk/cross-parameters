import QtQuick            2.15
import QtQuick.Controls   2.15
import QtQuick.Layouts    1.15
//import QtGraphicalEffects 1.15

// import Qt5Compat.GraphicalEffects

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

import "." as XG

Pane {

    id: _control;

    width: 300;
    height: window.height;

    /* modal: window.inPortrait */
    /* interactive: window.inPortrait */
    /* position: window.inPortrait ? 0 : 1 */
    /* visible: !window.inPortrait; */

    // edge: Qt.LeftEdge;

    function opened() { return _control.width != 0 }
    function open() { _control.width = 300; }
    function close() { _control.width = 0; }

    clip: true;

    Behavior on width {
        NumberAnimation {
            easing {
                type: Easing.OutElastic
                amplitude: 1.0
                period: 0.5
            }
        }
    }
//
    //Rectangle {
    //    color: "#00000000";
//
    //    anchors.fill: parent;
//
    //    visible: window.inPortrait;
//
    //    ShaderEffectSource {
    //        id: _source
//
    //        sourceItem: window
    //        width: parent.width;
    //        height: parent.height;
    //        sourceRect: Qt.rect(x,y, width, height)
    //    }
//
    //    FastBlur{
    //        id: _blur;
    //        anchors.fill: _source
    //        source: _source
    //        radius: 128
    //    }
//
        //ColorOverlay
       // {
        //    anchors.fill: parent
        //    source: _blur;
        //    color: "#ee000000";
       // }
    //}

    X.LabelHeadline5 {

        text: "Gallery";

        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.top: parent.top;
        anchors.topMargin: 42;
    }

    X.ButtonRound {

        id: _s_b;

        anchors.top: parent.top;
        anchors.topMargin: 10;
        anchors.right: parent.right;
        anchors.rightMargin: 10;

        iconSource: X.Icons.icons.close;

        onClicked: _drawer.close();

        height: 22;
        width: 48;

        visible: window.inPortrait;
    }


    X.Separator {
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: listView.top;
    }

    ListView {
        id: listView

        clip: true;
        focus: true
        currentIndex: -1
        anchors.topMargin: 100
        anchors.fill: parent

        delegate: ItemDelegate {
            width: listView.width
            text: model.title
            highlighted: ListView.isCurrentItem
            onClicked: {
                listView.currentIndex = index

                if(_stack.depth > 1)
                   _stack.pop();

                _footer.reset();

                _stack.push(_component, { source: model.source });

                if(inPortrait)
                   _drawer.close();
            }

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 0.8 : 0.3
                // color: Qt.darker(X.Style.backgroundColor, 1.2)
                //color: "#22000000";
                color: "#bb000000"

                Rectangle {
                    width: parent.width
                    height: 1
                    color: X.Style.borderColor;
                    anchors.bottom: parent.bottom
                }
            }
        }

        section.property: "section"
        section.criteria: ViewSection.FullString
        section.delegate: ItemDelegate {

            id: _section_delegate;

            required property string section

            width: listView.width
            text: section

            hoverEnabled: false;

            contentItem: Text {
                rightPadding: _section_delegate.spacing;
                text: _section_delegate.text;
                font: _section_delegate.font;
                color: X.Style.textColor;
                elide: Text.ElideRight;
                verticalAlignment: Text.AlignVCenter;
            }

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 0.8 : 0.3
                // color: Qt.darker(X.Style.backgroundColor, 1.2)
                color: "#11000000";

                Rectangle {
                    width: parent.width
                    height: 1
                    color: X.Style.borderColor;
                    anchors.bottom: parent.bottom
                }
            }
        }

        model: ListModel {

            id: _model;

            ListElement { section: "Widgets";    title: "Numeric"; source: "qrc:/examples/gallery/pages/NumericPage.qml" }
            ListElement { section: "Widgets";    title: "Slider"; source: "qrc:/examples/gallery/pages/SliderPage.qml" }
            ListElement { section: "Widgets";    title: "CheckBox"; source: "qrc:/examples/gallery/pages/CheckBoxPage.qml" }
            ListElement { section: "Widgets";    title: "Params"; source: "qrc:/examples/gallery/pages/ParamsPage.qml" }
            Component.onCompleted: {


                // if (QtVersion > '6') {

                //     _model.insert(0, {
                //         "section": "Modules",
                //         "title": "3D - Morphing",
                //         "source": "qrc:/examples/gallery/pages/3DMorphingPage.qml"
                //     });

                //     _model.insert(2, {
                //         "section": "Modules",
                //         "title": "3D - Morphing",
                //         "source": "qrc:/examples/gallery/pages/3DPrincipledMaterialPage.qml"
                //     });

                // }

                // if (X.Features.TTS != undefined)
                //     _model.append({
                //         "section": "Crates",
                //         "title": "TextToSpeech",
                //         "source": "qrc:/examples/gallery/pages/TextToSpeechPage.qml"
                //     });
                // else
                //     _model.append({
                //         "section": "Crates",
                //         "title": "TextToSpeech",
                //         "source": "qrc:/examples/gallery/pages/FeatureNotAvailablePage.qml"
                //     });

                // if (X.Features.VIS != undefined) {
                //     _model.append({
                //         "section": "Crates",
                //         "title": "Visualisation - Quadric",
                //         "source": "qrc:/examples/gallery/pages/VisualisationQuadricPage.qml"
                //     });

                //     _model.append({
                //         "section": "Crates",
                //         "title": "Visualisation - Sphere Source",
                //         "source": "qrc:/examples/gallery/pages/VisualisationSphereSourcePage.qml"
                //     });
                // } else {
                //     _model.append({
                //         "section": "Crates",
                //         "title": "Visualisation",
                //         "source": "qrc:/examples/gallery/pages/FeatureNotAvailablePage.qml"
                //     });
                // }
            }
        }

        ScrollIndicator.vertical: ScrollIndicator { }
    }

    X.Separator {

        anchors.top: parent.top;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;

        // visible: window.inPortrait;
    }
}
