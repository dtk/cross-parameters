use cpp::cpp;

use qmetaobject::prelude::*;

use x_logger;
use x_gui;
use x_quick;

cpp! {{
#include <crossParameters.hpp>
}}

qrc!(register_resources, "/" {
    "examples/gallery/qmldir",
    "examples/gallery/main.qml",
    "examples/gallery/main.header.qml",
    "examples/gallery/main.footer.qml",
    "examples/gallery/main.drawer.qml",
    "examples/gallery/main.settgs.qml",
    "examples/gallery/main.compnt.qml",
    "examples/gallery/pages/CheckBoxPage.qml",
    "examples/gallery/pages/SliderPage.qml",
    "examples/gallery/pages/NumericPage.qml",
    "examples/gallery/pages/ParamsPage.qml",
});

fn main() {
    x_logger::init();
    x_quick::init();
    cross_parameters::init();

    register_resources();

    let mut engine = x_gui::xApplicationEngine::new();
    engine.set_application_name("cross-parameters".into());
    engine.set_organisation_name("inria".into());
    engine.set_organisation_domain("fr".into());
    engine.add_import_path("qrc:///qml".into());
    engine.add_import_path("qrc:///examples/gallery".into());
    engine.set_property("_title".into(), engine.title().into());
    engine.set_source("qrc:///examples/gallery/main.qml".into());
    engine.resize(1024, 600);
    engine.exec();
}
