import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Component {

    Page {

        id: _page;

        property string source;

        property var object: undefined;

        SplitView {

            id: _split_container;

            anchors.fill: parent;

            Control {

                id: _placeholder;

                clip: true;

                SplitView.preferredWidth: _page.width / 2;
                SplitView.fillHeight: true;

                Loader {

                    id: _loader;

                    anchors.fill: parent;

                    source: _page.source;

                    Behavior on SplitView.preferredWidth {
                        NumberAnimation {
                            easing {
                                type: Easing.OutElastic
                                amplitude: 1.0
                                period: 0.5
                            }
                        }
                    }
                }
            }

            X.Editor {

                id: _editor;

                SplitView.fillHeight: true;
                SplitView.preferredWidth: _page.width / 2;

                X.EditorHelper {

                    id: _editor_helper;

                    Component.onCompleted: {
                        _editor.contents = read(_page.source);

                        setup(_editor.edit);
                    }

                    onLineChanged: {
                        _footer.line = line;
                    }
                    onColumnChanged: {
                        _footer.column = column;
                    }
                }

                X.SourceHighliter { Component.onCompleted: {
                        setup(_editor.document, X.Style.languages.qml);
                    }
                }

                Behavior on SplitView.preferredWidth {
                    NumberAnimation {
                        easing {
                            type: Easing.OutElastic
                            amplitude: 1.0
                            period: 0.5
                        }
                    }
                }
            }
        }

        Connections {

            target: _header;

            function onRun() {
                _loader.source = '';
                _loader.sourceComponent = undefined;

                if (_page.object != undefined)
                    _page.object.destroy();

                _editor.errors.clear();

                try {

                    object = Qt.createQmlObject(_editor.actualContents, _placeholder, "xQuickRuntime");
                    object.anchors.fill = _placeholder;

                } catch (exc) {

                    var errors = exc.qmlErrors;

                    for (var i = 0; i < errors.length; i++) {
                        var error = errors[i];

                        _editor.errors.append({
                            "line": error.lineNumber,
                            "message": " [" + error.lineNumber + ":" + error.columnNumber + "] - " + error.message
                        })

                        console.error("Build: [" + error.lineNumber + ":" + error.columnNumber + "] - " + error.message);
                    }
                }
            }
        }

        Component.onCompleted: {
            _footer.line = 1;
            _footer.column = 1;
            _footer.source = _page.source;
        }
    }
}
