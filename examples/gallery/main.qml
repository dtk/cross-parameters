import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xLogger         1.0 as L

import xQuick          1.0 as X
import xQuick.Controls 1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Style    1.0 as X

import "." as XG

import dtkCore         1.0 as D


X.Application {

    id: window;

    XG.GalleryHeader { id: _header;

        anchors.left: parent.left;
        anchors.leftMargin: inPortrait ? undefined : _drawer.width; // * _drawer.position;
        anchors.right: parent.right;

        // width: window.width - (_drawer.width * _drawer.position);
    }

    XG.GalleryFooter { id: _footer;

        anchors.left: parent.left;
        anchors.leftMargin: inPortrait ? undefined : _drawer.width; // * _drawer.position;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;

        // width: window.width - (_drawer.width * _drawer.position);

        onToggleConsole: {
            if (_console.height > 1)
                _console.SplitView.preferredHeight = 0;
            else
                _console.SplitView.preferredHeight = window.height * 1/4;
        }
    }

    XG.GalleryCompnt { id: _component; }

    X.SplitView {

        id: _split;

        visible: true;

        clip: true;

        orientation: Qt.Vertical;

        height: window.height - _header.height - _footer.height;

        anchors.fill: parent
        anchors.topMargin: _header.height
        anchors.leftMargin: !window.inPortrait ? _drawer.width : undefined
        anchors.bottomMargin: _footer.height

        /* anchors.fill: parent; */
        /* anchors.topMargin: _header.height; */
        /* anchors.bottomMargin: _footer.height; */
        /* anchors.leftMargin: inPortrait ? 0 : _drawer.width * _drawer.position; */

        handleFunc: function() {
            return _console.height > 2 ? X.Style.borderColor : "transparent";
        }

        StackView {
            id: _stack;

            SplitView.fillWidth: true;
            SplitView.fillHeight: true;

            clip: true;

            initialItem: Pane {

                Image {
                    id: _logo
                    width: 150;
                    height: 150;
                    anchors.centerIn: parent
                    anchors.verticalCenterOffset: -50
                    fillMode: Image.PreserveAspectFit;
                    source: "http://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Rust_programming_language_black_logo.svg/180px-Rust_programming_language_black_logo.svg.png";

                    // ColorOverlay
                    // {
                    //     anchors.fill: parent
                    //     source: _logo
                    //     color: X.Style.foregroundColor;
                    // }
                }

                Label {
                    text: "Qt Quick Controls provides a set of controls that can be used to build complete interfaces in Qt Quick."
                    anchors.margins: 20
                    anchors.top: _logo.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    horizontalAlignment: Label.AlignHCenter
                    verticalAlignment: Label.AlignVCenter
                    wrapMode: Label.Wrap
                }
            }
        }

        X.Console {

            id: _console;

            SplitView.fillWidth: true;
            SplitView.preferredHeight: 0; // window.height * 1/4;

            Behavior on SplitView.preferredHeight {
                NumberAnimation {
                    easing {
                        type: Easing.OutElastic
                        amplitude: 1.0
                        period: 0.5
                    }
                }
            }

            onItemsChanged: {

                if (L.Controller.items.length > 0)
                    SplitView.preferredHeight = window.height * 1/4;
                else
                    SplitView.preferredHeight = 0;
            }
        }

        background: Rectangle {
            //color: X.Style.tintColor;
            color: "#bb000000"
        }
    }

    XG.GalleryDrawer { id: _drawer; }
    XG.GallerySettgs { id: _settings; }

    Connections {
        target: _footer;

        function onTogglePage() {

            if(_stack.depth == 1)
                return;

            var left = _stack.currentItem.children[0].children[0].children[0].children[0];
            var right = _stack.currentItem.children[0].children[0].children[0].children[1];

            if (left.width > 1) {
                left.SplitView.fillWidth = false;
                left.SplitView.preferredWidth = 0;
                right.SplitView.fillWidth = true;
            } else {
                left.SplitView.preferredWidth = _stack.width / 2;
            }
        }

        function onToggleEditor() {

            if(_stack.depth == 1)
                return;

            var left = _stack.currentItem.children[0].children[0].children[0].children[0];
            var right = _stack.currentItem.children[0].children[0].children[0].children[1];

            if (right.width > 1) {
                left.SplitView.fillWidth = true;
                right.SplitView.fillWidth = false;
                right.SplitView.preferredWidth = 0;
            } else {
                right.SplitView.preferredWidth = _stack.width / 2;
            }
        }
    }

    Component.onCompleted: {

        _drawer.open();

        // console.info("Has feature Default:", X.Features.Default != undefined);
        // console.info("Has feature TTS:",     X.Features.TTS     != undefined);

        // console.info("Using QtVersion:", QtVersion);
        // console.info("QtVersion >  5 ?", QtVersion >  '5');
        // console.info("QtVersion >= 6 ?", QtVersion >= '6');
        // console.info("QtVersion <  6 ?", QtVersion <  '6');
    }
}
