import QtQuick          2.15
import QtQuick.Controls 2.15

import dtkCore         1.0 as D

Page {
    id: page

    D.Algorithm {
        id: _algorithm;
    }
    
    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Slider {
            id: _spin;
            from: 0
            to: 100
            anchors.centerIn: parent;

            value: _algorithm.param.value;

            onValueChanged: {
                console.info('Changing value' + _spin.value);
                _algorithm.param.value = _spin.value;
                _algorithm.print();
            }
        }
    }
}
