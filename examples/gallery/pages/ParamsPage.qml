import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import crossQuick      1.0 as C
import crossParameters 1.0 as C

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

Page {

    id: page

    C.CrossAlgorithmCommand {
        id: _algorithm;

        onInputChanged: {
            console.log(_algorithm.input);
        }

        onCollectionChanged: {
            params_model.clear();
            for (var param_name in _algorithm.collection) {
                var p = _algorithm.collection[param_name];
                var prop_dict = {};
                prop_dict["type"] = p.type;
                prop_dict["param"] = p;
                console.log("onCollectionChanged");
                console.log(param_name + "  -> " + JSON.stringify(prop_dict));
                params_model.append(prop_dict);
            }
        }
    }

    // Components
    Component {
        id: _dummy_component
        Text {text: lparam.label}
    }

    Component {
        id: _num_component
        C.Numeric {
            param: lparam
            paramType: dtype
        }
    }
    Component {
        id: _range_component
        C.Range {param: lparam}
    }

    Component {
        id: _string_component
        C.Simple {param: lparam}
    }

    Component {
        id: _path_component
        C.Path {param: lparam}
    }

    Component {
        id: _liststring_component
        C.InList {param: lparam}
    }

    Component {
        id: _liststringlist_component
        C.InListStringList {param: lparam}
    }

    ColumnLayout {

        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;
        anchors.margins: 10;

        spacing: 10;

        X.TextField {
            id: input_field
            placeholderText: "Input"
            selectByMouse: true

            Layout.fillWidth: true;
            Layout.preferredHeight: 40;
            Layout.alignment: Qt.AlignHCenter;

            onTextChanged: {
                _algorithm.input = text;
            }
        }

        ListView {

            id: _l;

            Layout.fillWidth: true;
            Layout.fillHeight: true;

            clip: true;

            model: ListModel {
                id: params_model;
                dynamicRoles: true;
            }

            delegate: Component {
                 Loader {
                    required property int index
                    property var lparam: params_model.get(index).param;
                    property string dtype: params_model.get(index).type;
                    height: 70;
                    width: _l.width;
                    sourceComponent: (dtype == "dtk::d_real" || dtype == "dtk::d_bool" || dtype == "dtk::d_int") ? _num_component :
                        (dtype == "dtk::d_range_real") ? _range_component:
                        (dtype == "dtk::d_string") ?  _string_component:
                        (dtype == "dtk::d_path") ? _path_component:
                        (dtype == "dtk::d_inliststring") ?  _liststring_component:
                        (dtype == "dtk::d_inliststringlist") ?  _liststringlist_component: _dummy_component
                }
            }

            ScrollIndicator.vertical: ScrollIndicator {
                visible: _l.contentHeight > _l.height;
            }
        }

        X.Button {
            id: _button
            text: "Run"

            Layout.fillWidth: false
            Layout.alignment: Qt.AlignHCenter;

            onClicked: {
                console.info(_algorithm.input);
                _algorithm.run();
                //output_field.text = _algorithm.output;
            }
        }

    }

    Component.onCompleted: {
        _algorithm.collectionChanged()
    }
}
