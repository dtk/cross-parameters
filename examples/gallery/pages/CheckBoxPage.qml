import QtQuick          2.15
import QtQuick.Controls 2.15

import dtkCore         1.0 as D

Page {

    id: page

    Column {
        spacing: 20
        anchors.horizontalCenter: parent.horizontalCenter

        CheckBox {
            text: "First"
            checked: true
        }
        CheckBox {
            text: "Second"
        }
        CheckBox {
            text: "Third"
            checked: true
            enabled: false
        }
    }
}

