import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

//import dtkCore         1.0 as D

import crossQuick      1.0 as C
import crossParameters 1.0 as C

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

Page {

    id: page

    C.CrossAlgorithmCommand {
        id: _algorithm;

        onInputChanged: {
            console.log(_algorithm.input);
        }
        input: input_field.text
    }

    ColumnLayout {

        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.margins: 10

        spacing: 10;

        X.TextField {
            id: input_field
            placeholderText: "Input"
            selectByMouse: true

            Layout.fillWidth: true;
            Layout.preferredHeight: 40;
            Layout.alignment: Qt.AlignHCenter;

        }

        C.Numeric {
            Layout.fillWidth: true;
            Layout.preferredHeight: 70;

            param: _algorithm.collection["r_freq"];
            paramType: param.type
        }

        C.Numeric {
            Layout.fillWidth: true;
            Layout.preferredHeight: 70;

            param: _algorithm.collection["i_size"];
            paramType: param.type
        }

        C.Numeric {
            Layout.fillWidth: true;
            Layout.preferredHeight: 70;

            param: _algorithm.collection["b_blank"];
            paramType: param.type
        }

        C.Range {
            Layout.fillWidth: true;
            Layout.preferredHeight: 80;

            param: _algorithm.collection["r_real"];
        }

        C.InList{
            Layout.fillWidth: true;
            Layout.preferredHeight: 70;

            param: _algorithm.collection["t_liststring"];
            }

        C.InListStringList {
            Layout.fillWidth: true;
            Layout.preferredHeight: 70;

            param: _algorithm.collection["w_inliststringlist"];
            }

        X.Button {
            id: _button
            text: "Run"

            Layout.fillWidth: false
            Layout.alignment: Qt.AlignHCenter;

            onClicked: {
                console.info(_algorithm.input);
                _algorithm.run();
                output_field.text = _algorithm.output;
            }
        }

        ToolSeparator { orientation: Qt.Horizontal; Layout.fillWidth: true }

        X.TextField {
            id: output_field

            Layout.fillWidth: true;
            Layout.preferredHeight: 40;
            Layout.alignment: Qt.AlignHCenter;

            readOnly: true;
        }
    }

    Component.onCompleted: {
        console.log("collection: " + JSON.stringify(_algorithm.collection))
        console.log("input: " + _algorithm.input)

        // console.info(Object.keys(_algorithm.parameters).length);

        // for(var i = 0; i < _algorithm.parameterList.length; i++) {
        //     console.info(_algorithm.parameterList[i], _algorithm.parameterList[i].value, _algorithm.parameterList[i].label);
        // }
        // for(var i = 0; i < Object.keys(_algorithm.parameters).length; i++) {
        //     console.info(Object.values(_algorithm.parameters)[i],
        //                  Object.values(_algorithm.parameters)[i].value,
        // 		 Object.values(_algorithm.parameters)[i].label);
        // }
        //console.warn("variantmap", JSON.stringify(_algorithm.parameters));
        //console.warn("JSValue", JSON.stringify(_algorithm.collection));

        // for(var key in _algorithm.parameters)
        //     console.warn(`${key}: ${_algorithm.parameters[key]} (${_algorithm.parameters[key].label})`);

        // for(var key in _algorithm.parametersOnceMore)
        //    console.warn(`${key}: ${_algorithm.parametersOnceMore[key]}`);

        // // NOTE: Next challenge ....
        // for(var i=0; i<_algorithm.parametersTwiceMore.length; i++)
        //     console.warn(_algorithm.parametersTwiceMore[i].label);
    }
}
