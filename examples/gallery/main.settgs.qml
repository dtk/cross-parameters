import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Dialog {

    id: settingsDialog

    x: Math.round((parent.width - width) / 2)
    y: Math.round(parent.height / 6)

    width: Math.round(Math.min(parent.width, parent.height) / 3 * 2)

    modal: true
    focus: true
    title: "Settings"

    standardButtons: Dialog.Ok | Dialog.Cancel

    leftMargin: 0;
    rightMargin: 0;

    ListView {

        Layout.leftMargin: 0;
        Layout.rightMargin: 0;
        Layout.fillWidth: true;

        SwitchDelegate {
            text: "Dark variant";
            checked: true;

            width: parent.width;

            onToggled: {
                X.Style.variant = (X.Style.variant == 'dark') ? 'light' : 'dark';
            }
        }
    }

    onAccepted: {
        settingsDialog.close()
    }

    onRejected: {
        settingsDialog.close()
    }

    contentItem: ColumnLayout {
        id: settingsColumn
        spacing: 20
    }
}
