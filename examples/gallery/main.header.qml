import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

ToolBar {

    height: 42;

    signal toggled();
    signal run();

    X.SegmentedCell {

        id: _cell;

        anchors.verticalCenter: parent.verticalCenter;
        anchors.right: parent.right;
        anchors.rightMargin: 10;

        width: _header.width * 3/4;
        height: 22;

        X.SegmentedCellSegment { icon.width: 12; iconSource: X.Icons.icons.view_sidebar; Layout.maximumWidth: 48;
            onClicked: {
                // if (_drawer.position > 0.1)
                if (_drawer.opened())
                    _drawer.close();
                else
                    _drawer.open();
            }
        }
        X.SegmentedCellSegment { icon.width: 12; iconSource: X.Icons.icons.play_arrow; Layout.maximumWidth: 48;
            onClicked: _header.run();
        }
        X.SegmentedCellSegment {
            X.LabelHint2 {
                anchors.left: parent.left;
                anchors.leftMargin: 10;
                anchors.top: parent.top;
                anchors.topMargin: 5;
                text: "Ready";
                height: _cell.height;
            }
        }
        X.SegmentedCellSegment { icon.width: 12; iconSource: X.Icons.icons.settings; Layout.maximumWidth: 48;

            onClicked: optionsMenu.open();

            Menu {
                id: optionsMenu
                x: parent.width - width
                y: _cell.height + 10;
                transformOrigin: Menu.TopRight

                MenuItem {
                    text: "Settings"
                    onTriggered: _settings.open()
                }
            }
        }
    }
}
